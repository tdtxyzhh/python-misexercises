<h1>Python_Turtle绘制正弦函数图像（采用函数描点法绘制）（turtle实现）</h1>

【注1】采用函数描点法绘制图像，最后一篇。要实现其他的函数图像使用turtle海龟库绘制，在代码中替换成相应的函数表达式即可。

【注2】采用点动成线的原理绘制，密集的描点即可！
# 1.SinDot.py
```python
import turtle
import math
turtle.speed(0)
turtle.delay(0)
turtle.tracer(False)
i=-150
while(i<=150):
    i=i+0.1
    turtle.penup()
    y = 60*math.sin(0.06*i+0.5)+20
    turtle.goto(i,y)
    turtle.pendown()
    turtle.dot(4)
turtle.penup()
turtle.goto(0,0)
turtle.pendown()
turtle.seth(90)
turtle.fd(230)
turtle.seth(-70)
turtle.fd(15)
turtle.penup()
turtle.goto(0,230)
turtle.pendown()
turtle.seth(-110)
turtle.fd(15)
turtle.penup()
turtle.goto(0,0)
turtle.pendown()
turtle.seth(-90)
turtle.fd(90)
turtle.penup()
turtle.goto(0,0)
turtle.pendown()
turtle.seth(0)
turtle.fd(150)
turtle.seth(-160)
turtle.fd(15)
turtle.penup()
turtle.goto(150,0)
turtle.pendown()
turtle.seth(160)
turtle.fd(15)
turtle.penup()
turtle.goto(0,0)
turtle.pendown()
turtle.seth(-180)
turtle.fd(150)
turtle.penup()
turtle.goto(0,-120)
turtle.write("正弦函数方程图像:y = 60*math.sin(0.06*i+0.5)+20",True,align="center")
turtle.goto(0,-300)
turtle.done()
```
# 2.结果示例
![在这里插入图片描述](https://img-blog.csdnimg.cn/5ae69261f40b44bdb42de548d16515e9.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAVERUWA==,size_20,color_FFFFFF,t_70,g_se,x_16)





