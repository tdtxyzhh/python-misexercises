<h1>Python_真值表求解器（逻辑表达式的求解）（eval实现求解）</h1>

【注】实现了一个**输入逻辑表达式，求解其真值表——真值表求解器**。

**实现思路：**

 **1.输入检查。** 

 **2.提取表达式中的不同变量，放入lvar列表中。** 

 **3.按照lvar中变量的个数，生成变量的真值组合，采用生成集合全部子集的手段实现。** 

 **4.将生成的真值组合每组替换相应的变量，在将该字符串放入eval函数中即可得到该组合下的逻辑值，最后形成结果列表，输出即可。** 

【1可输入逻辑符号】**&&、||、！**

【2可输入的逻辑变量】 **a--z(不能输入a、n、d、n、o、t、r)** 只能英文小写字符

【3可输入的运算优先级括号】**只能是()**

【4其他可输入字符】只有**空格**

**除上述1234所指的字符，都是非法输入，且逻辑符号是一对的但是只输入了一个也视为错误输入！**


**【注】该功能之后会集成在MathToolsPaint中。**

MathToolsPaint回顾：

[Python_数学绘图工具MathToolsPaint[项目]（动态组件与PaneWindow）（tkinter实现）](https://gitee.com/tdtxyzhh/python-misexercises/blob/master/%E6%95%B0%E5%AD%A6%E7%BB%98%E5%9B%BE%E5%B7%A5%E5%85%B7MathToolsPaint%5B%E8%AF%A6%E7%BB%86%E6%96%87%E7%AB%A0%5D.md)

# 1.TruthValueSolver.py
```python
s = input("输入一个逻辑表达式：")
#print("s = ",s)
ts = s

#开始输入检查
no_yes = True
i = 0

varset = ['b','c','e','f','g','h','i','j','k','l','m','p','q','s','u','v','w','x','y','z']

while i < len(s):
    if (s[i] not in varset) and s[i] not in ['(',')',' ']:
        if s[i] not in ['&','|','!']:
            print("逻辑表达式[非规定字符]输入错误！[发生错误位置]-->",i)
            no_yes = False
            break
        else:
            if s[i+1] != s[i] and i + 1 < len(s) - 1 and s[i] != '!':
                print("逻辑表达式[运算符]输入错误！[发生错误位置]-->",i)
                no_yes
                no_yes = False
                break
            else:
                i = i + 2
    else:
        i = i + 1
print("检查结果-->",no_yes)
#完成输入检查

if no_yes == True:
    #开始提取所有不同的变量符号，放入列表lvar中
    lvar = []
    for i in list(s):
        if (i >= 'a' and i <= 'z') and (i not in lvar):
            lvar.append(i)
    lvar_len = len(lvar)
    print("\n提取的变量lvar-->",lvar)
    #提取所有不同的变量符号完成

    #开始生成变量符号的真值组合，即生成含有lvar_len个元素集合的全部子集
    print("\n输出变量真值组合：")
    lt = []
    max = 2**lvar_len
    for i in range(0,max):
        tlt = list(bin(i))
        tlt.pop(0)
        tlt.pop(0)
        tlt_len = len(tlt)
        for j in range(0,lvar_len - tlt_len):
            tlt.insert(0,'0')
        print(tlt)
        lt.append(tlt)
    #print("lt = ",lt)
    #生成变量符号的真值组合完成

    #开始将生成的真值组合中的每一组，带入到相应的变量值，调用eval函数得到表达式真值
        #并添加到结果列表s_result,同时输出该表达式及其值
    print("\n输出变量真值组合的值：")
    s_result = []
    for i in lt:
        ts = s
        ts = ts.replace('&&',' and ')  #将&&、||、!替换成python的逻辑运算符
        ts = ts.replace('||',' or ')
        ts = ts.replace('!',' not ')
        
        for j in range(0,lvar_len):
            #print(ts,lvar[j],i[j])
            ts = ts.replace(lvar[j],i[j]) #将每个变量的位置用对应的真值组合中的0或1代替
        s_result.append(int(eval(ts))) #使用eval函数计算
        print(ts,":-->",int(eval(ts)))
    #生成该逻辑表达式真值表完成

    #开始输出真值表
    k = 0
    print("\n\t\t<<<真值表>>>")
    for i in lvar:
        print(i,"\t",end='')
    print(s,"")

    for i in lt:
        for j in range(0,lvar_len):
            print(i[j],"\t",end='')
        print(s_result[k])
        k = k + 1
    #输出真值表完成
```

# 2.结果示例
## 2.1 含有非规定字符的输入

![在这里插入图片描述](https://img-blog.csdnimg.cn/06d46e049ac94b3e8f6e5196dcf4afeb.png)

## 2.2 &&、||不完整输入
![在这里插入图片描述](https://img-blog.csdnimg.cn/7c0b5696c0b14e6991097fc97295f14b.png)

## 2.3 正确输入与结果
(q && p || q) && !w

![在这里插入图片描述](https://img-blog.csdnimg.cn/23743941de1b4f3398ddbc705452421e.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAVERUWA==,size_20,color_FFFFFF,t_70,g_se,x_16)

m || (q && p || q) && !w

![在这里插入图片描述](https://img-blog.csdnimg.cn/b77c529ba7294ded85cb84b98cfddd42.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAVERUWA==,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)











