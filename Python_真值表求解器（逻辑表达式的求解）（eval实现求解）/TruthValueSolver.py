s = input("输入一个逻辑表达式：")
#print("s = ",s)
ts = s

#开始输入检查
no_yes = True
i = 0

varset = ['b','c','e','f','g','h','i','j','k','l','m','p','q','s','u','v','w','x','y','z']

while i < len(s):
    if (s[i] not in varset) and s[i] not in ['(',')',' ']:
        if s[i] not in ['&','|','!']:
            print("逻辑表达式[非规定字符]输入错误！[发生错误位置]-->",i)
            no_yes = False
            break
        else:
            if s[i+1] != s[i] and i + 1 < len(s) - 1 and s[i] != '!':
                print("逻辑表达式[运算符]输入错误！[发生错误位置]-->",i)
                no_yes
                no_yes = False
                break
            else:
                i = i + 2
    else:
        i = i + 1
print("检查结果-->",no_yes)
#完成输入检查

if no_yes == True:
    #开始提取所有不同的变量符号，放入列表lvar中
    lvar = []
    for i in list(s):
        if (i >= 'a' and i <= 'z') and (i not in lvar):
            lvar.append(i)
    lvar_len = len(lvar)
    print("\n提取的变量lvar-->",lvar)
    #提取所有不同的变量符号完成

    #开始生成变量符号的真值组合，即生成含有lvar_len个元素集合的全部子集
    print("\n输出变量真值组合：")
    lt = []
    max = 2**lvar_len
    for i in range(0,max):
        tlt = list(bin(i))
        tlt.pop(0)
        tlt.pop(0)
        tlt_len = len(tlt)
        for j in range(0,lvar_len - tlt_len):
            tlt.insert(0,'0')
        print(tlt)
        lt.append(tlt)
    #print("lt = ",lt)
    #生成变量符号的真值组合完成

    #开始将生成的真值组合中的每一组，带入到相应的变量值，调用eval函数得到表达式真值
        #并添加到结果列表s_result,同时输出该表达式及其值
    print("\n输出变量真值组合的值：")
    s_result = []
    for i in lt:
        ts = s
        ts = ts.replace('&&',' and ')  #将&&、||、!替换成python的逻辑运算符
        ts = ts.replace('||',' or ')
        ts = ts.replace('!',' not ')
        
        for j in range(0,lvar_len):
            #print(ts,lvar[j],i[j])
            ts = ts.replace(lvar[j],i[j]) #将每个变量的位置用对应的真值组合中的0或1代替
        s_result.append(int(eval(ts))) #使用eval函数计算
        print(ts,":-->",int(eval(ts)))
    #生成该逻辑表达式真值表完成

    #开始输出真值表
    k = 0
    print("\n\t\t<<<真值表>>>")
    for i in lvar:
        print(i,"\t",end='')
    print(s,"")

    for i in lt:
        for j in range(0,lvar_len):
            print(i[j],"\t",end='')
        print(s_result[k])
        k = k + 1
    #输出真值表完成

        
