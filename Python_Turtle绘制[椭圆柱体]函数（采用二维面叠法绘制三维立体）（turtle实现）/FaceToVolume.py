import turtle as tle
import math

def turtleSet():  
    tle.speed(0)
    tle.delay(0)
    tle.pensize(2)
    tle.pencolor("blue")
    #tle.tracer(True)
    tle.tracer(False)

def CreateXYZ():
    tle.pencolor("black")
    tle.penup()
    tle.goto(0,0)
    tle.seth(0)
    tle.pendown()
    tle.goto(200,0)
    tle.seth(135)
    tle.fd(10)
    tle.penup()
    tle.goto(200,0)
    tle.pendown()
    tle.seth(-135)
    tle.fd(10)
    tle.penup()
    tle.goto(203,0)
    tle.write("x(X)",font=("宋体",16,"normal"))

    tle.penup()
    tle.goto(0,0)
    tle.seth(0)
    tle.pendown()
    tle.goto(-200,0)

    tle.penup()
    tle.goto(0,0)
    tle.seth(45)
    tle.pendown()
    tle.fd(210)
    tle.penup()
    tle.fd(3)
    tle.write("y",font=("宋体",16,"normal"))
    tle.bk(3)
    tle.pendown()
    tle.seth(-180)
    tle.fd(10)
    tle.penup()
    tle.bk(10)
    tle.pendown()
    tle.seth(-90)
    tle.fd(10)
    tle.penup()
    tle.bk(10)

    tle.penup()
    tle.goto(0,0)
    tle.seth(-135)
    tle.pendown()
    tle.fd(200)

    tle.penup()
    tle.goto(0,0)
    tle.seth(90)
    tle.pendown()
    tle.fd(200)
    tle.penup()
    tle.fd(3)
    tle.write("z(Y)",font=("宋体",16,"normal"))
    tle.pendown()
    tle.seth(-135)
    tle.fd(10)
    tle.penup()
    tle.bk(10)
    tle.pendown()
    tle.seth(-45)
    tle.fd(10)
    tle.penup()
    tle.bk(10)

    tle.penup()
    tle.goto(0,0)
    tle.seth(-90)
    tle.pendown()
    tle.fd(200)

    tle.penup()

def subline(x,coll,z,y=[]):
    tle.pencolor(coll)
    tle.penup()

    for y0 in y:
        if math.fabs(y0 - z) <= 0.0001:
            #print("OK",x,y0,z)
            tle.goto(x,0+z)
            tle.pendown()
            tle.dot(1)
            tle.penup()
        else:
            tle.goto(x,0+z)
            tle.pendown()
            tle.seth(45)
            tle.fd(y0/2)
            tle.bk(y0)
            tle.penup()

def subface(z,coll):
    k = 0
    i = -150

    while(i<=150):
        if k == 1:
            k = 0
        y = []
        y.append(math.sqrt(2500-i*i*(1/9)))

        subline(i,coll,z,y)
        i = i + 1
        k = k + 1

def CreateV():
    z = -50
    col = ["blue","green","orange"]
    k = 0
    while z <= 50:
        if k == 3:
            k = 0
        if z == -50 or z == 50:
            subface(z,"green")
        else:
            subface(z,col[k])
        z=z+2
        k = k + 1

turtleSet()
CreateXYZ()
CreateV()
CreateXYZ()

tle.done()