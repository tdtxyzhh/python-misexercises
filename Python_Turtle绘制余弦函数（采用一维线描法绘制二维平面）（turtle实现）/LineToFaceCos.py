import turtle as tle
import math
tle.speed(0)
tle.delay(0)
tle.pensize(1)
tle.pencolor("blue")
tle.tracer(True)
#tle.tracer(False)

col = ["blue","green"]

def subline(x,coll,y=[]):
    tle.pencolor(coll)
    tle.penup()
    tle.goto(x,-220)
    tle.pendown()
    for i in y:
        tle.goto(x,i-2)
        tle.penup()
        tle.goto(x,i+2)
        tle.pendown()
    tle.goto(x,220)

k = 0
i = -150
while(i<=150):
    if k == 2:
        k = 0
    i = i + 1
    y = []
    y.append(70*math.cos(0.06*i+4)+20)

    '''
    y.append(math.sqrt(22500-i*i))
    y.append(-math.sqrt(22500-i*i))
    print(y[0])
    print(y[1])
    '''
    subline(i,col[k],y)
    
    k = k + 1
    
tle.done()
