import turtle as tle
import math
tle.speed(0)
tle.delay(0)
tle.pensize(2)
tle.pencolor("blue")
tle.tracer(True)
#tle.tracer(False)

col = ["yellow"]

def subline(x,coll,y=[]):
    tle.pencolor(coll)
    tle.penup()
    tle.goto(x,-220)
    tle.pendown()
    for i in y:
        if x >= -150 and x <-60:
            tle.goto(x,i-1.4)
            #tle.penup()
            tle.pencolor("red")
            tle.goto(x,i+1.4)
            tle.pencolor(coll)
            #tle.pendown()
        elif x >= -60 and x <= 0:
            tle.goto(x,i-0.8)
            #tle.penup()
            tle.pencolor("red")
            tle.goto(x,i+0.8)
            tle.pencolor(coll)
            #tle.pendown()
        if x > 0 and x <= 60:
            tle.goto(x,i-0.8)
            #tle.penup()
            tle.pencolor("red")
            tle.goto(x,i+0.8)
            tle.pencolor(coll)
            #tle.pendown()
        elif x >= 60 and x <= 150:
            tle.goto(x,i-1.4)
            #tle.penup()
            tle.pencolor("red")
            tle.goto(x,i+1.4)
            tle.pencolor(coll)
            #tle.pendown()
        
    tle.goto(x,220)
    
k = 0
i = -150

while(i<=150):
    if k == 1:
        k = 0
    y = []
    if i == 150 or i == -150:
        y.append(math.sqrt(22500-i*i))
    else:
        y.append(-math.sqrt(22500-i*i))
        y.append(math.sqrt(22500-i*i))
    subline(i,col[k],y)
    
    i = i + 1
    k = k + 1

tle.done()